<?php

namespace Scara\Recaptcha;

use Scara\Support\Facades\Facade;

/**
 * Recaptcha class's facade.
 */
class RecaptchaFacade extends Facade
{
    /**
     * Registers the Recaptcha facade accessor.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'recaptcha';
    }
}
