<?php

namespace Scara\Recaptcha;

use Curl\Curl;
use Philo\Blade\Blade;

/**
 * The Recaptcha class
 */
class Recaptcha
{
    /**
     * Recaptcha site key.
     *
     * @var string
     */
    private $_sitekey;

    /**
     * Recaptcha secret key.
     *
     * @var string
     */
    private $_secretkey;

    /**
     * Allow remote IP sending to Google.
     *
     * @var bool
     */
    private $_remoteip = false;

    /**
     * Recaptcha verification URL.
     *
     * @var string
     */
    private $_url = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * cURL instance.
     *
     * @var \Curl\Curl
     */
    private $_curl;

    /**
     * cURL response.
     *
     * @var stdObject
     */
    private $_response;

    /**
     * Blade instance.
     *
     * @var \Philo\Blade\Blade
     */
    private $_blade;

    /**
     * Blade view instance.
     *
     * @var \Illuminate\View\Factory
     */
    private $_view = null;

    /**
     * Class constructor.
     *
     * @param array $config
     *
     * @return void
     */
    public function __construct($config = [])
    {
        if (!empty($config)) {
            if (isset($config['sitekey'])) {
                $this->_sitekey = $config['sitekey'];
            }

            if (isset($config['secretkey'])) {
                $this->_secretkey = $config['secretkey'];
            }

            if (isset($config['remoteip'])) {
                $this->_remoteip = $config['remoteip'];
            }
        }

        $this->_curl = new Curl();
        $this->_blade = new Blade(__DIR__.'/views', __DIR__.'/views/cache');
    }

    /**
     * Sets the site key.
     *
     * @param string $key
     *
     * @return void
     */
    public function setSiteKey($key)
    {
        $this->_sitekey = $key;
    }

    /**
     * Sets the secret key.
     *
     * @param string $key
     *
     * @return void
     */
    public function setSecretKey($key)
    {
        $this->_secretkey = $key;
    }

    /**
     * Renders the Recaptcha view.
     *
     * @return string
     */
    public function render()
    {
        if ($this->_view == null) {
            $this->_view = $this->_blade->view()->make('recaptcha')->with('sitekey', $this->_sitekey);
        }

        return $this->_view->render();
    }

    /**
     * Resets the Recaptcha view.
     *
     * @param bool $rerender
     *
     * @return mixed
     */
    public function reset($rerender = false)
    {
        $this->_view = null;

        if ($rerender) {
            return $this->render();
        }
    }

    /**
     * Validates the supplied recaptcha response.
     *
     * @param string $rc
     *
     * @return bool
     */
    public function validate($rc)
    {
        if (!empty($rc)) {
            $options = [
                'secret' => $this->_secretkey,
                'response' => $rc,
            ];

            if ($this->_remoteip) {
                $options['remoteip'] = $_SERVER['REMOTE_ADDR'];
            }

            $this->_curl->post($this->_url, $options);
            $this->_response = $this->_curl->response;

            return $this->_response->success;
        }

        return false;
    }

    /**
     * Gets error codes in array format.
     *
     * @return array
     */
    public function getErrors()
    {
        $ar = (array)$this->_response;
        return $ar['error-codes'];
    }
}
