<?php

namespace Scara\Recaptcha;

use Scara\Support\ServiceProvider;
use Scara\Config\Configuration;

/**
 * Recaptcha Service Provider.
 */
class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Registers the Recaptcha's service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('recaptcha', function()
        {
            $c = new Configuration();
            $cc = $c->from('recaptcha');
            return new Recaptcha([
                'sitekey'   => $cc->get('sitekey'),
                'secretkey' => $cc->get('secretkey'),
                'remoteip'  => $cc->get('remoteip'),
            ]);
        });
    }
}
