<?php

return [
    // your recaptcha site key
    'sitekey'   => '',

    // your recaptcha secret key
    'secretkey' => '',

    // send google the remote ip?
    'remoteip'  => false
];
